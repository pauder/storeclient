<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';
include_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

function storeclient_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event)
{
	bab_functionality::includeOriginal('Icons');
	
	if (bab_isUserAdministrator()) {
		$position = array('root', 'DGAll', 'babAdmin', 'babAdminSection', 'babAdminInstall');
		
		$item = $event->createItem('storeclientList');
		
		$item->setLabel(storeclient_translate('Install from remote server'));
		$item->setDescription(storeclient_translate('Install or upgrade package from remote server'));
		$item->setLink(storeclient_Controller()->Admin()->displaylist()->url());
		$item->addIconClassname(Func_Icons::ACTIONS_LIST_ADD);
		$item->setPosition($position);
		
		$event->addFunction($item);
	}
}


/**
 * Daily task
 * @param LibTimer_eventDaily $event
 */
function storeclient_onDaily(LibTimer_eventDaily $event)
{
	$registry = bab_getRegistry();
	$registry->changeDirectory('/bab/install_repository/');
	
	$autoupgrade = (bool) $registry->getValue('autoupgrade');
	
	if ($autoupgrade) {
		
	}
}

/*
 * Redirect on first install
 * @param bab_eventPageRefreshed $event
 */
function storeclient_firstInstall(bab_eventPageRefreshed $event) {
    
    if (bab_isUserAdministrator()) {

        $addon = bab_getAddonInfosInstance('storeclient');
        
        // true will be available on success from version 8.4.98
        if (true === $addon->removeEventListener('bab_eventPageRefreshed', 'storeclient_firstInstall', 'events.php')) {
        
            $url = new bab_url();
            $url->tg = 'addon/storeclient/main';
            $url->idx = 'admin.displaylist';
            $url->tag = 'recommend';
            $url->location();
        }
    }
}
