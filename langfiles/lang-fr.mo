��    $      <  5   \      0     1  F   8          �     �     �     �     �     �     �     �  -        E  %   _     �     �     �     �     �  3   �  "        )     B     [  	   `  -   j  /   �  F   �  D     
   T     _     {  %   �  Q   �       M       g	  \   n	     �	     �	  	   �	  $   
     +
     <
     I
  	   R
     \
  5   x
  &   �
  )   �
     �
  !        4     <     @  N   R  '   �  *   �  &   �          '  -   6  0   d  W   �  g   �     U  (   ]  (   �  .   �  ;   �                                   	          $                                                            "      
              #                                                    !        Author Automatically upgrade the installed packages on a daily scheduled task Available version Check installed addons Continue Current installed version: %s Current version Dependencies Details Install Install from remote server Install or upgrade package from remote server Install selected packages Installation of the selected packages Installed version Last available version: %s License Name Nothing to install Package %s is not installable, check version number Package %s not found in repository Packages available on %s Root archives folder url Save Store url The recommanded version is allready installed The recommended version %s is already installed This url must point to a json-encoded file with the available packages This version contains only bug fixes released since your last update Up to date Upgrade to the last version Upgrade to the latest version Upgrade to the recommended version %s Url of the base folder containing archives. If empty, the store url will be used. Version Project-Id-Version: storeclient
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-03-12 11:33+0100
PO-Revision-Date: 2019-03-12 11:33+0100
Last-Translator: Laurent Choulette <laurent.choulette@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: storeclient_translate;storeclient_translate:1,2
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: programs
 Auteur Mettre à jour automatiquement les modules installés avec une tâche planifiée quotidienne Version disponible Cocher les modules installés Continuer Version actuellement installée : %s Version actuelle Dépendences Détails Installer Installer depuis un serveur Installer ou mettre à jour depuis un serveur distant Installer les archives sélectionnées Installation les archives sélectionnées Version installée Dernière version disponible : %s Licence Nom Rien à installer L'archive %s n'est pas installable, vous devez vérifier le numéro de version Archive %s non trouvée dans le dépôt Archives d'installation disponibles sur %s Url du répertoire racine des archives Enregistrer Url du "store" La version recommandée est déjà installée La version recommandée %s est déjà installée Cette URL doit pointer vers un fichier json contenant la liste des archives disponibles Cette version contient uniquement des corrections de bugs publiées depuis votre dernière mise à jour À jour Mettre à jour vers la dernière version Mettre à jour vers la dernière version Mettre à jour vers la version recommandée %s Laisser vide pour utiliser la même url que celle du store. Version 